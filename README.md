![The Fruit Flinger at Connections Church](fruit-flinger.png)
# The Fruit Flinger at Connections Church

a.k.a. Punkin Chunkin

## Important

0. Someone should act as range safety officer(RSO). The RSO is the boss.
1. If anyone is in front of the launchers, loudly close the range by saying, "The range is closed!"
2. Keep the splice of the power-bands near the ammo pocket, so that if/when the splice fails, the power bands move toward the poles, and the operator doesn't get a nasty welt.

## Setup

### Setup the targets

For each target:

1. Pound two rebar halfway into the ground, 12-18" further apart than the target is wide.
2. Set a PVC pipe on each rebar.
3. Attaching a target to the PVC pipes with bungie cord. Secure the top tightly, and the bottom loosely.

### Setup the launcher

1. Attach the power bands to the poles using the spring-clips.
2. Ensure that the splices of the power-bands, which are the most likely failure point, are near the pocket, so that if/when they break or release, the power-bands contract toward the poles, not the person operating the fruit flinger.
3. Encircle the launch area with cones and tape, so that there is plenty of room to pull back on the pocket.
4. Put the ammunition (pumpkins and water balloons) outside the launch area, in the queue area.
